#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "md5.h"
#include <string.h>


int main(int argc, char *argv[])
{
 if(argc < 3)
    {
       printf("You need to supply two filenames\n");
       exit(1);
    }

   FILE *src = fopen(argv[1], "r");
   if(!src)
   {
    perror("cant open source file");
    exit(1);
   }
   
   FILE *dest = fopen(argv[2], "w");
   if(!dest)
    {
    perror("cant open dest file");
    exit(1);
    }
    
     char line[33];
     char* pwds;
   while (fgets(line, 33, src) != NULL)
      {
      pwds =  md5(line, strlen(line)-1);
      fprintf(dest, "%s\n", pwds);
      free(pwds);
      }
      
}
      
      